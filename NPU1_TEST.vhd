-- Test-bench for Neuron PROCESSing Unit
-- A shallow neural network for two classes 
-- By Jeng-Hau Lin
-- 2017-05-22

----------------------------------------
-- Include libraries
LIBRARY ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use STD.textio.all;

----------------------------------------
-- A test-bench has no port
ENTITY NPU1_TEST IS
END NPU1_TEST;
 
----------------------------------------
-- Use mixed modelings to implement the test-bench
ARCHITECTURE beha OF NPU1_TEST IS 
  -- Read the trained model parameters
  signal TW: std_logic_vector(3 DOWNTO 0) := (others=>'0');
  signal Tk: std_logic_vector(7 DOWNTO 0) := (others=>'0');
  signal Th: std_logic_vector(7 DOWNTO 0) := (others=>'0');
  -- Read the features
  type X_t is array (0 TO 29, 3 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
  signal test_X: X_t := (others=>(others=>"00000000"));
  -- Read the labels
  type y_t is array (0 TO 29) of std_logic;
  signal test_y: y_t := (others=>'0');
  -- The neuron PROCESSing unit dealing with high precision fixed numbers
  COMPONENT NPU1
  port(CLK: in std_logic;
    A: in  std_logic_vector(31 DOWNTO 0); -- match the 4 8-bit features 
    W: in  std_logic_vector(3 DOWNTO 0);
    k: in  std_logic_vector(7 DOWNTO 0);
    h: in  std_logic_vector(7 DOWNTO 0);
    P: out std_logic);
  end COMPONENT;
  -- Clock and data
  constant Tpe: time := 2 ns; -- clock period
  signal TCLK: std_logic:= '0'; -- clock
  signal TA: std_logic_vector(31 DOWNTO 0); -- input all 4 8-bit features of a sample
  signal TP: std_logic; -- results showing whether the sample belongs to the first kind of iris.
  -- an impure function for file reading
  impure FUNCTION read_file(
  str: string;
  str_len: integer;
  nbit: integer;
  matrix_w: integer;
  matrix_h: integer)
  return std_logic_vector is
    variable  bin_value: std_logic_vector(nbit*(matrix_w)*matrix_h-1 DOWNTO 0);
    file file_pointer: text;
    variable line_content: string(1 to nbit*matrix_w);
    variable line_num: line;
    variable j: integer := 0; -- character counter in the file
    variable r: integer := 0; -- row counter in the file
    variable char: character; 
  BEGIN
    file_open(file_pointer,str(1 TO str_len),READ_MODE);	  
    while not endfile(file_pointer) loop 
      readline (file_pointer,line_num);
      read(line_num,line_content);  
      for j in 1 to (nbit*matrix_w) loop		
        char := line_content(j);
        if(char = '0') then
          bin_value((nbit*matrix_w*r) + nbit*matrix_w-j) := '0';
        else
          bin_value((nbit*matrix_w*r) + nbit*matrix_w-j) := '1';
        end if;	
      end loop;
      r := r + 1;
    end loop;
    file_close(file_pointer);
    return bin_value;
  end FUNCTION read_file;
BEGIN
  -- Clock generator
  clk_gen : PROCESS
  BEGIN
    TCLK <= not TCLK;
    wait for Tpe/2;
  end PROCESS clk_gen;

  -- Read the trained model parameters and the dataset
  read_weights: PROCESS  
    -- Model parameter
    variable wfilename: string(1 to 30) := "data/model_shallow/weights.txt";
    variable w_slv: std_logic_vector(3 DOWNTO 0);
    variable khfilename: string(1 to 25) := "data/model_shallow/kh.txt";
    variable kh_slv: std_logic_vector(8*2-1 DOWNTO 0) := (others=>'0');
    -- Dataset
    variable Xfilename: string(1 to 24) := "data/test_set/test_X.txt";
    variable test_X_slv: std_logic_vector(8*4*30-1 DOWNTO 0);
    variable yfilename: string(1 to 24) := "data/test_set/test_y.txt";
    variable test_y_slv: std_logic_vector(1*1*30-1 DOWNTO 0);
  BEGIN
    w_slv := read_file(wfilename, 30, 1,4,1);
    TW <= w_slv;
    kh_slv := read_file(khfilename, 25, 8,2,1);
    Tk <= kh_slv(15 DOWNTO 8); 
    Th <= kh_slv(7 DOWNTO 0);

    test_X_slv := read_file(Xfilename, 24, 8,4,30);
    test_y_slv := read_file(yfilename, 24, 1,1,30);
    for r in 0 TO 29 loop
      test_X(r,3) <= test_X_slv(r*8*4+31 DOWNTO r*8*4+24); -- the 4 features in 8-bit signed format
      test_X(r,2) <= test_X_slv(r*8*4+23 DOWNTO r*8*4+16);
      test_X(r,1) <= test_X_slv(r*8*4+15 DOWNTO r*8*4+8);
      test_X(r,0) <= test_X_slv(r*8*4+7  DOWNTO r*8*4+0);

      test_y(r) <= test_y_slv(r);
    end loop;
    wait;
  end PROCESS read_weights;

  -- Main program
  -- Instantiate a shallow network composed with only one neuron
  DUT: NPU1 port map (TCLK, TA, TW, Tk, Th, TP);

  -- Feed the samples into the network
  shallow: PROCESS
    variable error_count: integer := 0;
  BEGIN
  wait for 2 ns; -- Wait for all file readings to finish

    for i in 0 to 29 loop -- iterate over all 30 samples in the test set
      TA(31 DOWNTO 24) <= Test_X(i,3);
      TA(23 DOWNTO 16) <= Test_X(i,2);
      TA(15 DOWNTO 8) <= Test_X(i,1);
      TA(7 DOWNTO 0) <= Test_X(i,0);

      wait for 2 ns;
      if (TP /= test_y(i)) then -- Check the classification correctness
        error_count := error_count +1;
      end if;
      report "Target: " & std_logic'image(test_y(i)) & ", Predicted: " & std_logic'image(TP);
    end loop;
    report "Total error count: " & integer'image(error_count) & "/30"; -- the error rate should be 6/30
    wait;
  end PROCESS shallow;

end beha;
