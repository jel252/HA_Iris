-- Input Neuron Processing Unit
-- Take 8-bit fixed point input, return binarized activation
-- By Jeng-Hau Lin
-- 2017-05-22

----------------------------------------
-- Include libraries
LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----------------------------------------
-- A NPU has 6 ports
ENTITY NPU1 is
port(CLK: in std_logic;
       A: in std_logic_vector(31 DOWNTO 0);
       W: in std_logic_vector(3 DOWNTO 0);
       k: in std_logic_vector(7 DOWNTO 0);
       h: in std_logic_vector(7 DOWNTO 0);
       P: out std_logic);
end NPU1;

----------------------------------------
-- Use data-flow modeling to implement the NPU
ARCHITECTURE internal of NPU1 is
  constant uMAX: unsigned(7 DOWNTO 0):= "11111111";
  constant pMAX: signed(7 DOWNTO 0):= "01111111";
  constant nMAX: signed(7 DOWNTO 0):= "10000000";

  -- We need a FUNCTION to take care of overflow in addition
  -- Any overflow due to value greate then +127 returns +127
  -- Any overflow due to value smaller then -128 returns -128
  FUNCTION sat_add(
    FA: signed(7 DOWNTO 0);
    FB: signed(7 DOWNTO 0)) 
  return signed is
    variable VS: signed(7 DOWNTO 0):= (others=>'0');
    variable VIS: integer := 0;
  BEGIN
    VS := FA) + FB;
    if (VS(7) /= FA(7)) and (VS(7) /= FB(7)) and FA(7)='0' then
      VS := pMAX;
    elsif (VS(7) /= FA(7)) and (VS(7) /= FB(7)) and FA(7)='1' then
      VS := nMAX;
    end if;

    -- [Conversion through integer is Acceptable]
    --VIS := to_integer(FA + FB);
    --if VIS> 127 then
    --    VS := to_signed(127,8);
    --elsif VIS < -128 then
    --    VS := to_signed(-128,8);
    --else
    --    VS := to_signed(VIS,8);
    --end if;

    return VS;
  end FUNCTION sat_add;

  -- We need a FUNCTION to take care of overflow in multiplication
  -- Any overflow due to value greate then +127 returns +127
  -- Any overflow due to value smaller then -128 returns -128
  -- Trim the first 4 bits and last 4 bits of the 16-bit product 
  FUNCTION sat_mul(
    FA: signed(7 DOWNTO 0);
    FB: signed(7 DOWNTO 0))
  return signed is
    variable VP: signed(15 DOWNTO 0):= (others=>'0');
    variable VPP: signed(7 DOWNTO 0):= (others=>'0');
    variable VIP: integer :=0;
  BEGIN
    VP := FA * FB;
    if (VP(15) = '0') and (unsigned(VP(11 DOWNTO 4))>unsigned(pMAX)) then
      VPP := pMAX;
    elsif (VP(15) = '0') and (unsigned(VP(11 DOWNTO 4))<=unsigned(pMAX)) then
      VPP := '0' & VP(10 DOWNTO 4);
    elsif (VP(15) = '1') and (unsigned(VP(11 DOWNTO 4))>=unsigned(pMAX)) then
      VPP := '1' & VP(10 DOWNTO 4);
    elsif (VP(15) = '1') and (unsigned(VP(11 DOWNTO 4))<unsigned(pMAX)) then
      VPP := nMAX;
    end if;

    -- [Conversion through integer is Acceptable]
    --VP := FA*FB;
    --VIP := to_integer(VP(15 DOWNTO 4));
    --if VIP > 127 then
    --    VPP := to_signed(127,8);
    --elsif VIP < -128 then
    --    VPP := to_signed(-128,8);
    --else
    --    VPP := to_signed(VIP,8);
    --end if;

    return VPP;
  end FUNCTION sat_mul;
BEGIN

  PROCESS(CLK)
    variable prod1: signed(7 DOWNTO 0) := "00000000";
    variable sum1: signed(7 DOWNTO 0) := "00000000";
    variable sum1_scale: signed(7 DOWNTO 0) := "00000000";
    variable sum1_nmld: signed(7 DOWNTO 0) := "00000000";
    variable error_count: integer := 0;
  BEGIN
    if rising_edge(CLK) then
      sum1 := (others=>'0'); -- initialize the sum for dot-product between input and weights
      
      -- in Binarized Neural Network, the dot-product is modified to avoid O(N^2) multiplication
      -- where N is the number of neurons on a layer
      for j in 3 downto 0 loop
        prod1 := signed(A(((j+1)*8-1) DOWNTO (j)*8));
           
        if (W(j)='0') then -- negate the sign if weight is 0
          prod1 := "11111111" - prod1 + "00000001"; -- 2'complement sign toggling
        end if;

        -- [Conversion through integer is Acceptable]
        --if (W(j)='0') then -- negate the sign if weight is 0
        --  prod1 := to_signed(0-to_integer(prod1),8);
        --end if;

        sum1 := sat_add(sum1, prod1); -- use our customized adder to accumulate
      end loop;
        
      sum1_scale := sat_mul(sum1,signed(k)); -- use our customized multiplier to scale
      sum1_nmld := sat_add(sum1_scale,signed(h)); -- use our customized adder to shift

      if(sum1_nmld > to_signed(0 ,8)) then -- output 1 if greater then the threshold
        P <= '1';
      else -- output 0 if smaller than or equal to the threshold
        P<= '0';
      end if;
    end if;

  end PROCESS;
end internal;
