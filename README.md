## A Hardware Accelerator for Classifying Iris Dataset

I use Courbariaux's BNN[1] to train two very small models for the classification on Iris dataset. Then I quantize all parameters except for the already binarized weights. 
The quantifization and validation is done with Python codes. 
With the trained and quantified parameters, I built VHDL codes for the forward propagation part of the two BNNs.
This project has been used as my students final project in CSE143 at UCSD.
As long as you cite this repository and the BNN paper, you are free to use my code for any non-commericial purpose.

## Project Introduction

As AlphaGo keeps callously defeating human Go game masters, the computer scientists of DeepMind are simplifying its training and inference algorithm to carry out a portable, single machine AlphaGo. In this lab assignment, we will learn to build neural networks for low-cost hardware with minimal number of multipliers using VHDL. The goal is to gain experience on hardware accelerator for deep learning. More specifically, we will build a neuromorphic hard- ware neural
network with VHDL.
Dataset: iris
In this lab assignment, we use iris dataset. Each sample is compound by four features: sepal length, sepal width, petal length, and petal width. The goal is to predict that the features belong to which kind of the three irises. The dataset includes 150 samples, and we already use 120 samples for the off-line training. To avoid peeking the answer, there are 30 samples hold out for testing.
Given Package: NPU_pkg
1. data/test_set/test_X.txt (for both P1 & P2): 30 rows of 32-bit binary number. The size of test set is 30, and each sample contains 4 features, which is converted into a 8-bit fixed number.
2. data/test_set/test_y.txt (for P1): 30 rows of 1-bit binary number. The label of the first kind of iris.
3. data/model_shallow/weights.txt (for P1): 1 rows of 4-bit binary number. The trained weights for the shallow binarized  neural network.
4. data/model_shallow/kh.txt (for P1): 1 row of 8-bit binary number. The scaling (k) and shifting (h) para- meters for the shallow binarized neural network.
5. data/test_set/test_y_3c (for P2).txt 30 rows of integer, of which the value ranges from 0 to 2. The 3 labels of the test set. For the deep binarized neural network, we change to classify feature vectors into one of the three classes.
6. data/model_deep/weights0.txt (for P2): 50 rows of 4-bit binary number. The trained weights for the hidden layer of the deep binarized neural network.
7. data/model_deep/weights1.txt (for P2): 3 rows of 50-bit binary number. The trained weights for the out- put layer of the deep binarized neural network.
8. data/model_deep_kh0.txt (for P2): 50 rows of 16-bit binary number. The trained scaling and shifting parameters for the 50 hidden neurons. On each row, the first 8 bits stand for the scaling parameter, and the last 8 bits are used as the shifting parameter.
9. data/model_deep_kh1.txt (for P2): 50 rows of 16-bit binary number. The trained scaling and shifting parameters for the 3 output neurons. On each row, the first 8 bits stand for the scaling parameter, and the last 8 bits are used as the shifting parameter.
10. NPU1.vhd: the source library with some missing blocks. 
11. NPU1_TEST.vhd: the test-bench for NPU1.vhd . 

## Problem 1. A shallow binarized neural network
As shown in the source library and the test-bench, we built a shallow neural network to recognize the first class in iris dataset. In “data/test_set/test_y.txt”, we have marked the first kind of iris to be “1”, and convert the labels of the other two classes to be “0”. The feature is converted from floating-point numbers to signed 8-bit fixed-point numbers. During the conversion, we multiply the floating-point numbers by 16 to shift the decimal point to right 4 times, so that we can promote the precision.

The shallow network has been trained to learn the single class classification task. If you fill in the three missing codes in NPU1.vhd correctly. The output sequence of NPU1 in the test-bench NPU1_TEST.vhd should be similar with that in “test_y.txt”. There exists certain error rate (20%) for this oversimplified network. Figure P1.1 illustrate the network structure for the shallow network.
<br><img src="https://gitlab.com/jel252/HA_Iris/raw/master/Img/Fig1_1.png"  width="50%" ><br>
The three missing codes are marked as <<P1.1>>, <<P1.2>>, and <<P1.3>> in NPU1.vhd. P1.1: Implement the code for a saturated adder to handle overflow. It takes two 8-bit signed inputs (FA and FB) and return one 8-bit signed output (VS).
P1.2: Implement the code for a saturated multiplier to handle overflow. It takes two 8-bit signed inputs (FA and FB) and return one 8-bit signed output (VPP).
P1.3: Implement the core of binarized neural network to inverse the sign of incoming signal. In- vert the flip the sign of variable (prod1) if the matching weight (W(j)) is ‘0’. The the saturated adder will take care of the accumulation.
After filling the three missing blocks, simulate NPU1_TEST.vhd for 100 ns and check the Total Error Rate printed in the transcript window.
Requirement:
1. Process all 30 samples wishing 100ns.
2. Do not modify NPU1_TEST.vhd .
3. Do not modify NPU1.vhd except the three missing blocks.
4. Do not modify test_X.txt, test_y.txt, weights.txt, and kh.txt.
5. The paths of the given files must starts from “data”, e.g., “data/model_shallow/kh.txt”. 

## Problem 2. A deep binarized neural network
Use 50 NPU1 from Problem 1 to build the hidden layer of a deep binarized neural network in a new test-bench
(DBNN1_TEST.vhd). Build 3 output neurons with your new neural processing unit NPU2 (NPU2.vhd) for the output layer.
NPU2 takes 50 1-bit inputs and 50 1-bit inputs and return 1 8-bit signed number. Theoretically the core operation should
be simpler than NPU1, and the output is the sum1_nmld without any thresholding. We perform the class inference in
DBNN1_TEST.vhd through an argmax proce- dure to check which neuron output the highest degree of belief. If, for example,
the first neuron produces the highest signed number, we classify the concerned sample to be of class 0.
In DBNN1_TEST.vhd, modify the test-bench from Problem 1 to read in the four files in data/ model_deep/ as the model
parameters. Read in test_X.txt and test_y_3c.txt as your input and target pair. You need to implement another impure
function to read integers in the 3-class label file, test_y_3c.txt. The goal is to get the error rate of your deep
network.
There are 50 neurons (NPU1) on the hidden layer and 3 output neurons (NPU2) on the output layer. Figure P2.1 shows the
network structure of the deep network. Your network must process the 30 samples within 200 ns, and the final message on
the transcript must be the error rate (expected to be 3/30 or 10%).
<br><img src="https://gitlab.com/jel252/HA_Iris/raw/master/Img/Fig2_1.png"  width="50%" ><br>
Table P2.1 lists the port definition of NPU2.
<br><img src="https://gitlab.com/jel252/HA_Iris/raw/master/Img/Tab2_1.png"  width="50%" ><br>

Requirement:
Table P2.1
1. Do not modify weight0.txt, weight1.txt, kh0.txt, kh1.txt, test_X.txt, and test_y_3c.txt.
2. Process all 30 samples wishing 200 ns.
3. Follow the coding style in the 2 given VHDL files to help the graders to trace your code. For example, use the same
   indentation, and similar naming rule.
4. The last reported message in the transcript must be your error rate.
5. The paths of the given files must starts from “data”, e.g., “data/model_deep/kh0.txt”.

Hints:
1. Use structural modeling to build the neural network in DBNN1_TEST.vhd as what we did in Lab1.SEVENBIT_FA.
2. Make use of the function read_file(.) to read in all the 4 files in data/model_deep/. To read test_y.txt, you need to implement another function reading integers form 0 to 2.
3. Trace the two given VHDL code NPU1.vhd and NPU1_TEST.vhd, and make sure you understand all concepts regarding VHDL and deep learning.
4. If you are confused by the matrices’ dimensions during accessing files, inspect the given text files and have a look at the first page of this assignment again.
5. The output neuron NPU2 is almost the same with NPU1, but the output neuron requires some modification of the ports because it takes 50 1-bit features from the hidden layer and generate a 8-bit fixed number as the output.
6. In your main process in DBNN1_TEST.vhd, you need to write some code to perform argmax on the three 8-bit degrees of belief from the three output neurons. The decide the classification result according to the highest degree of belief. Do not forget to accumulate error counts and report the error rate in the transcript window.
7. While collecting hidden neuron’s outputs, you must reverse entire vector if you are using keyword DOWNTO to define the signal.
    
## Further readings:
    [1] Hubara, I., Courbariaux, M., Soudry, D., El-Yaniv, R. and Bengio, Y., 2016. Binarized neural networks. In
    Advances in Neural Information Processing Systems (pp. 4107-4115).
    [2] https://github.com/MatthieuCourbariaux/BinaryNet
    [3] Ioffe, Sergey, and Christian Szegedy. "Batch normalization: Accelerating deep network train- ing by reducing
    internal covariate shift." arXiv preprint arXiv:1502.03167 (2015).
