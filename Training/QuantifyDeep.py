#====================#
# 2017-05-22
# Credit to Courbariaux's binary_net on gitHhub: https://github.com/MatthieuCourbariaux/BinaryNet
# Modified by Jeng-Hau Lin for UCSD CSE143 Lab4
# The is the training for the single neuron single class classification
#====================#
#from __future__ import print_function

import sys
import os
import time

import numpy as np
import random
random.seed(1234)
np.random.seed(1234)  # for reproducibility

from pylearn2.datasets.iris import Iris

import readNpz04
import math

if __name__ == "__main__":
    print('Loading Iris dataset...')
    idx = range(150)
    random.shuffle(idx)

    #idx_tr = idx[:110]
    #idx_va = idx[110:130] # split 20 samples for validation
    idx_tr = idx[:120]
    idx_va = idx[120:] # validation set is the same with the test set
    idx_te = idx[120:]

    train_set = Iris()
    valid_set = Iris()
    test_set = Iris()
    data = Iris()

    print "data.X.shape: ", data.X.shape # (150,4)
    print "data.y.shape: ", data.y.shape # (150,1)
    #print "data.X", data.X # the three classes are not shuffled
    #print "data.y", data.y # the three classes are not shuffled

    train_set.X, train_set.y = data.X[idx_tr, :], data.y[idx_tr, :]
    valid_set.X, valid_set.y = data.X[idx_va, :], data.y[idx_va, :]
    test_set.X, test_set.y = data.X[idx_te, :], data.y[idx_te, :]
    """
    train_set.X = train_set.X.reshape(-1, 1, 1, 4)
    valid_set.X = valid_set.X.reshape(-1, 1, 1, 4)
    test_set.X = test_set.X.reshape(-1, 1, 1, 4)
    """
    train_set.X =0.25* train_set.X.reshape(-1, 1, 1, 4) - 1.
    valid_set.X = 0.25* valid_set.X.reshape(-1, 1, 1, 4) - 1.
    test_set.X = 0.25* test_set.X.reshape(-1, 1, 1, 4) - 1.
    """
    train_set.X = bina(train_set.X)
    valid_set.X = bina(valid_set.X)
    test_set.X = bina(test_set.X)
    """
    train_set.X = np.float32(train_set.X)
    valid_set.X = np.float32(valid_set.X)
    test_set.X = np.float32(test_set.X)

    print(train_set.X.shape)
    #print train_set.X
    print "min: ",np.amin(train_set.X)
    print "max: ",np.amax(train_set.X)
    print(train_set.y.shape)
    #assert False

    """
    # Now we recoginze only the first class
    first = train_set.y<1
    rest = train_set.y>0
    train_set.y[first] = 1.0
    train_set.y[rest] =  0.0
    first = valid_set.y<1
    rest = valid_set.y>0
    valid_set.y[first] = 1.0
    valid_set.y[rest] =  0.0
    test_set.y[first] = 1.0
    test_set.y[rest] =  0.0
    #print "single class train_set.y: ", train_set.y
    """
    """
    # flatten targets
    train_set.y = np.hstack(train_set.y)
    valid_set.y = np.hstack(valid_set.y)
    test_set.y = np.hstack(test_set.y)
    """
    # Onehot the targets
    """
    train_set.y = np.float32(np.eye(3)[train_set.y])    
    valid_set.y = np.float32(np.eye(3)[valid_set.y])
    test_set.y = np.float32(np.eye(3)[test_set.y])
    """
    """
    train_set.y = np.float32(np.eye(1)[train_set.y])    
    valid_set.y = np.float32(np.eye(1)[valid_set.y])
    test_set.y = np.float32(np.eye(1)[test_set.y])
    """

    train_set.y = np.float32(train_set.y)    
    valid_set.y = np.float32(valid_set.y)
    test_set.y = np.float32(test_set.y)

    print(train_set.X.shape)
    print(train_set.y.shape)
    """
    # For shallow
    #print(train_set.y)
    std = 0.386136
    mean = 0.751844
    ga = 1.00466
    be = -2.29287

    #sum1 = np.sum(a)
    #print "sum1= ", sum1
    epsilon = 1e-4
    k = ga / math.sqrt(std**2.0+epsilon)
    h = be - k*mean
    #k = 1.333125
    #h = -0.42999375
    print "k: ", k,", h: ", h
    w = np.array([1.0,1.0,-1.0,-1.0])
    """
    """
    #write test set to two files
    text_file = open("test_X.txt", "w")
    counter = 1
    for sample in test_set.X:
        arr1 =  sample.flatten()
        sum1 =  np.dot(arr1,w)
        pred = sum1*k+h
        print counter,": ", pred
        counter = counter +1


        for feature in sample.flatten():
            text_file.write("%s" % readNpz04.twosCom(readNpz04.fixedInt(feature)))
        text_file.write("\n")
    text_file.close()
    
    text_file = open("test_y.txt", "w")
    for label in test_set.y.flatten():
        text_file.write("%s\n" % int(label))
    text_file.close()
    """
    with np.load('iris_deep.npz') as f_bsf: 
        param_values = [f_bsf['arr_%d' % i] for i in range(len(f_bsf.files))] 

    epsilon = 1e-4
    weight0 = (param_values[0])
    weight1 = (param_values[5])
    be0 = param_values[1]
    ga0 = param_values[2]
    mean0 = param_values[3]
    std0 = 1./param_values[4]
    k0 = ga0 / np.sqrt(np.power(std0,2.0)+epsilon)
    h0 = be0 - k0*mean0
    print "std0: ", std0.max()
    print "mean0: ", mean0.max()
    print "ga0: ", ga0.max()
    print "be0: ",be0.max()
    print "k0: ",k0.max()
    print "h0: ",h0.max()

    be1 = param_values[6]
    ga1 = param_values[7]
    mean1 = param_values[8]
    std1 = 1.0/param_values[9]
    k1 = ga1 / np.sqrt(np.power(std1,2.0)+epsilon)
    h1 = be1 - k1*mean1
    print "k1: ",k1
    print "h1: ",h1

    # binarization
    weight0 = np.where(weight0>0., 1., -1.)
    #print "weight0: ", weight0
    print "weight0: ", weight0.shape
    weight1 = np.where(weight1>0., 1., -1.)
    #print "weight1: ", weight1
    print "weight1: ", weight1.shape

    #print "test_set.X[:2]: ", test_set.X[:2]
    test_set.X = test_set.X.flatten().reshape(-1,4)
    #print "test_set.X[:2]: ", test_set.X[:2]
    print "test_set.X:", test_set.X.shape
    #print "X.max: ", test_set.X.max 
    hid_fea = np.matmul(test_set.X, weight0)
    print "hid_fea: ", hid_fea.shape
    #print "hid_fea: ", np.where(hid_fea[0][:20]>0., 1., 0.)
    #print "hid_fea: ", np.where(hid_fea>0., 1., 0.)[0]
    #assert False
    

    hid_act = np.zeros_like(hid_fea)
    for i in range(30):
        vec = hid_fea[i]
        vec_scale = vec*k0
        vec_shift = vec_scale+h0
        hid_act[i] = vec_shift
    print "hid_act: ", hid_act.shape
    print "hid_act: ", np.max(hid_act)
    """
    #print "hid_act: ", hid_act[0][:20]
    print "weight1: ", weight1.transpose()[1][:10]
    print "weight1: ", weight1.transpose()[1][10:20]
    print "weight1: ", weight1.transpose()[1][20:30]
    print "weight1: ", weight1.transpose()[1][30:40]
    print "weight1: ", weight1.transpose()[1][40:50]
    print "hid_act: ", np.where(hid_act[0][:10]>0., 1., 0.)
    print "hid_act: ", np.where(hid_act[0][10:20]>0., 1., 0.)
    print "hid_act: ", np.where(hid_act[0][20:30]>0., 1., 0.)
    print "hid_act: ", np.where(hid_act[0][30:40]>0., 1., 0.)
    print "hid_act: ", np.where(hid_act[0][40:50]>0., 1., 0.)
    """
    #assert False
    hid_act = np.where(hid_act>0., 1., -1.)

    out_fea = np.matmul(hid_act, weight1)
    #print "out_fea: ", out_fea[:10]
    #print "out_fea: ", np.where(out_fea[0]>0., 1., 0.)
    #ans1 = np.dot(hid_act[0], weight1.transpose()[0])
    #print "ans1 = ", ans1
    #assert False
    out_act = np.zeros_like(out_fea)
    print "out_fea.max: ", out_fea.max()
    print "k1: ", k1
    print "h1: ", h1
    for i in range(30):
        vec = out_fea[i]
        vec_scale = vec*k1
        vec_shift = vec_scale + h1
        out_act[i] = vec_shift
    print "out_fea: ", out_fea[:10]
    #print "out_act: ", out_act.shape
    print "out_act: ", out_act[:10]*16
    #assert False
    #print "out_act: ", out_act
    pred = np.argmax(out_act, axis=1)
    print pred

    
    # Write labels to test_y.txt
    text_file = open("test_y_3c.txt", "w")
    for label in test_set.y.flatten():
        text_file.write("%s\n" % int(label))
    text_file.close()
    
    text_file = open("weights0.txt", "w")
    weight0 = weight0.transpose()
    w = np.where(weight0>0.,weight0,0)
    for r,_ in enumerate(w):
        for c in w[r]:
            text_file.write("%s" % int(c))
        text_file.write("\n")
    text_file.close()

    text_file = open("weights1.txt", "w")
    weight1 = weight1.transpose()
    w = np.where(weight1>0.,weight1,0)
    for r,_ in enumerate(w):
        for c in w[r]:
            text_file.write("%s" % int(c))
        text_file.write("\n")
    text_file.close()
    
    text_file = open("kh0.txt", "w")
    for r,_ in enumerate(k0):
        text_file.write("%s" % readNpz04.twosCom(readNpz04.fixedInt(k0[r])))
        text_file.write("%s\n" % readNpz04.twosCom(readNpz04.fixedInt(h0[r])))
    text_file.close()

    text_file = open("kh1.txt", "w")
    for r,_ in enumerate(k1):
        text_file.write("%s" % readNpz04.twosCom(readNpz04.fixedInt(k1[r]*16)))
        text_file.write("%s\n" % readNpz04.twosCom(readNpz04.fixedInt(h1[r])))
    text_file.close()





