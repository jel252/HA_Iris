import numpy as np
import math

def twosCom(a):
    num_bits = 8
    return bin(a % (1<<num_bits))[2:].zfill(num_bits)

def rev_twosCom(a):
    x = int (a,2)
    num_bits = 8
    n = 0
    if x > ((1<<(num_bits-1))-1):
        n = 1 <<num_bits
    return x - n

def fixedInt(a):
    if a>(127.0/16.0):
        a = 127.0/16.0
    elif a < (-128.0/16.0):
        a = -128.0/16.0
    #b = int(round(a * 16.0))
    b = int(math.floor(a * 16.0))
    return b

def rev_fixedInt(a):
    b = float(a) / 16.0
    return b

def bina(a):
    b = 1
    if a<=0:
        b = int(0)
    return b

if __name__ == "__main__":
    #with np.load('mnist_parameters.npz') as f_bsf: 
    with np.load('iris_deep.npz') as f_bsf: 
        param_values = [f_bsf['arr_%d' % i] for i in range(len(f_bsf.files))] 
    
    #print param_values
    print  len(param_values)

    for i in range(len(param_values)):
            print i,": ", param_values[i].shape
    #assert False

    print "0: ", param_values[0].flatten()
    print "1: ", param_values[1].flatten()
    print "2: ", param_values[2].flatten()
    print "3: ", param_values[3].flatten()
    print "4: ", param_values[4].flatten()
    

    """
    print "test 1: ", rev_twosCom(twosCom(1))
    print "test 127: ", rev_twosCom(twosCom(127))
    print "test -1: ", rev_twosCom(twosCom(-1))
    print "test -128: ", rev_twosCom(twosCom(-128))
    """
    my_test_list = [1.1, 3.15, 4.04, 0.06, 17.2, -17.2]
    
    for i in my_test_list:
        a = fixedInt(i)
        b = twosCom(a)
        c = rev_twosCom(b)
        d = rev_fixedInt(c)
        print i, " fixedInt: ", a, ", 2's: ", b, ", rev_2's: ", c, ", rev_fix: ", d
    """
    print "fixedInt(1.1): ",  fixedInt(1.1) , ", 2's: ", twosCom(fixedInt(1.1))
    print "fixedInt(3.15): ", fixedInt(3.15), ", 2's: ", twosCom(fixedInt(3.15))
    print "fixedInt(4.04): ", fixedInt(4.04), ", 2's: ", twosCom(fixedInt(4.04))
    print "fixedInt(0.06): ", fixedInt(0.06), ", 2's: ", twosCom(fixedInt(0.06))
    """

    std = param_values[1][0]

    mean = param_values[2][0]

    ga = param_values[3][0]

    be = -param_values[4][0]
    print "std: ", std 
    print "mean: ", mean
    print "ga: ", ga
    print "be: ", be

    
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))
    
    k = ga/std
    print "k: ", k
    h = be - k*mean
    print "h: ", h
    
    text_file = open("weights.txt", "w")
    for r in param_values[0]:
        for c in param_values[0][r]:
            text_file.write("%s" % bina(i))
        text_file.write("\n")
    text_file.close()
    
    text_file = open("kh.txt", "w")
    #for r in param_values[0]:

    text_file.write("%s" % twosCom(fixedInt(k)))
    text_file.write("%s\n" % twosCom(fixedInt(h)))
    text_file.close()
