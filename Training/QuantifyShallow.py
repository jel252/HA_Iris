#====================#
# 2017-05-22
# Credit to Courbariaux's binary_net on gitHhub: https://github.com/MatthieuCourbariaux/BinaryNet
# Modified by Jeng-Hau Lin for UCSD CSE143 Lab4
# The is the training for the single neuron single class classification
#====================#
#from __future__ import print_function

import sys
import os
import time

import numpy as np
import random
random.seed(1234)
np.random.seed(1234)  # for reproducibility

from pylearn2.datasets.iris import Iris

import readNpz04
import math

if __name__ == "__main__":
    print('Loading Iris dataset...')
    idx = range(150)
    random.shuffle(idx)

    #idx_tr = idx[:110]
    #idx_va = idx[110:130] # split 20 samples for validation
    idx_tr = idx[:120]
    idx_va = idx[120:] # validation set is the same with the test set
    idx_te = idx[120:]

    train_set = Iris()
    valid_set = Iris()
    test_set = Iris()
    data = Iris()

    print "data.X.shape: ", data.X.shape # (150,4)
    print "data.y.shape: ", data.y.shape # (150,1)
    #print "data.X", data.X # the three classes are not shuffled
    #print "data.y", data.y # the three classes are not shuffled

    train_set.X, train_set.y = data.X[idx_tr, :], data.y[idx_tr, :]
    valid_set.X, valid_set.y = data.X[idx_va, :], data.y[idx_va, :]
    test_set.X, test_set.y = data.X[idx_te, :], data.y[idx_te, :]
    """
    train_set.X = train_set.X.reshape(-1, 1, 1, 4)
    valid_set.X = valid_set.X.reshape(-1, 1, 1, 4)
    test_set.X = test_set.X.reshape(-1, 1, 1, 4)
    """
    train_set.X =0.25* train_set.X.reshape(-1, 1, 1, 4) - 1.
    valid_set.X = 0.25* valid_set.X.reshape(-1, 1, 1, 4) - 1.
    test_set.X = 0.25* test_set.X.reshape(-1, 1, 1, 4) - 1.
    """
    train_set.X = bina(train_set.X)
    valid_set.X = bina(valid_set.X)
    test_set.X = bina(test_set.X)
    """
    train_set.X = np.float32(train_set.X)
    valid_set.X = np.float32(valid_set.X)
    test_set.X = np.float32(test_set.X)

    print(train_set.X.shape)
    #print train_set.X
    print "min: ",np.amin(train_set.X)
    print "max: ",np.amax(train_set.X)
    print(train_set.y.shape)
    #assert False

    # Now we recoginze only the first class
    first = train_set.y<1
    rest = train_set.y>0
    train_set.y[first] = 1.0
    train_set.y[rest] =  0.0
    first = valid_set.y<1
    rest = valid_set.y>0
    valid_set.y[first] = 1.0
    valid_set.y[rest] =  0.0
    test_set.y[first] = 1.0
    test_set.y[rest] =  0.0
    #print "single class train_set.y: ", train_set.y
    """
    # flatten targets
    train_set.y = np.hstack(train_set.y)
    valid_set.y = np.hstack(valid_set.y)
    test_set.y = np.hstack(test_set.y)
    """
    # Onehot the targets
    """
    train_set.y = np.float32(np.eye(3)[train_set.y])    
    valid_set.y = np.float32(np.eye(3)[valid_set.y])
    test_set.y = np.float32(np.eye(3)[test_set.y])
    """
    """
    train_set.y = np.float32(np.eye(1)[train_set.y])    
    valid_set.y = np.float32(np.eye(1)[valid_set.y])
    test_set.y = np.float32(np.eye(1)[test_set.y])
    """

    train_set.y = np.float32(train_set.y)    
    valid_set.y = np.float32(valid_set.y)
    test_set.y = np.float32(test_set.y)

    print(train_set.X.shape)
    print(train_set.y.shape)
    
    with np.load('iris_shallow.npz') as f_bsf: 
        param_values = [f_bsf['arr_%d' % i] for i in range(len(f_bsf.files))] 

    epsilon = 1e-4
    weight = (param_values[0])
    w = np.where(weight>0., 1., -1.)
    
    # the correct seqence on official site
    be = param_values[1]
    ga = param_values[2]
    mean = param_values[3]
    std = 1./param_values[4]
    
    k = ga / math.sqrt(std**2.0+epsilon)
    h = be - k*mean
    print "std: ", std
    print "mean: ", mean
    print "ga: ", ga
    print "be: ",be
    print "k: ",k
    print "h: ",h

    test_set.X = test_set.X.flatten().reshape(-1,4)
    hid_fea = np.matmul(test_set.X, w)
    print "hid_fea: ", hid_fea.shape
    hid_act = np.zeros_like(hid_fea)
    for i in range(30):
        vec = hid_fea[i]
        vec_scale = vec*k
        vec_shift = vec_scale+h
        hid_act[i] = vec_shift
        print i+1, ": ", vec_shift[0]>0
    #print np.where(hid_act>0.)
    #print hid_act

    
    # Write features to test_X.txt
    text_file = open("test_X.txt", "w")
    counter = 1
    for sample in test_set.X:
        for feature in sample:
            text_file.write("%s" % readNpz04.twosCom(readNpz04.fixedInt(feature)))
        text_file.write("\n")
    text_file.close()

    # Write labels to test_y.txt
    text_file = open("test_y.txt", "w")
    for label in test_set.y.flatten():
        text_file.write("%s\n" % int(label))
    text_file.close()
   
    text_file = open("weights.txt", "w")
    w = w.transpose()
    w = np.where(w>0.,w,0)
    for r,_ in enumerate(w):
        for c in w[r]:
            text_file.write("%s" % int(c))
    text_file.close()
    
    text_file = open("kh.txt", "w")
    for r,_ in enumerate(k):
        text_file.write("%s" % readNpz04.twosCom(readNpz04.fixedInt(k[r])))
        text_file.write("%s\n" % readNpz04.twosCom(readNpz04.fixedInt(h[r])))
    text_file.close()
