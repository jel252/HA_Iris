#====================#
# 2017-05-22
# Credit to Courbariaux's binary_net on gitHhub: https://github.com/MatthieuCourbariaux/BinaryNet
# Modified by Jeng-Hau Lin for UCSD CSE143 Lab4
# The is the training for the single neuron single class classification
#====================#
#from __future__ import print_function

import sys
import os
import time

import numpy as np
import random
random.seed(1234)
np.random.seed(1234)  # for reproducibility

# specifying the gpu to use
import theano.sandbox.cuda
theano.sandbox.cuda.use('gpu1') 
import theano
import theano.tensor as T

import lasagne

import binary_net

from pylearn2.datasets.iris import Iris
from pylearn2.utils import serial

from collections import OrderedDict

def bina(A):
    A[A>0] = 1.
    A[A<=0] = -1.
    return A

#==== JH: Given a test set and a model, this function tests the model on the entire test set ====#
#====     This function is copied from the Test-time folder ====#
def test(
        val_fn,
        batch_size,
        X_test,y_test):
        
    # This function tests the model a full epoch (on the whole dataset)
    def val_epoch(X,y):
        
        err = 0
        loss = 0
        batches = len(X)/batch_size
        
        for i in range(batches):
            new_loss, new_err = val_fn(X[i*batch_size:(i+1)*batch_size], y[i*batch_size:(i+1)*batch_size])
            print "Error:", new_err
            err += new_err
            loss += new_loss
        
        err = err / batches * 100
        loss /= batches

        return err, loss
    
    
    # We iterate over epochs:
        
    start_time = time.time()
    test_err, test_loss = val_epoch(X_test,y_test)
    epoch_duration = time.time() - start_time
    
    # Then we print the results for this epoch:
    print "="*20
    print("Epoch took "+str(epoch_duration)+"s")
    print("  test loss:                     "+str(test_loss))
    print("  test error rate:               "+str(test_err)+"%") 
    sys.stdout.flush()

if __name__ == "__main__":
    
    # BN parameters
    batch_size = 10
    print("batch_size = "+str(batch_size))
    # alpha is the exponential moving average factor
    alpha = .1
    print("alpha = "+str(alpha))
    epsilon = 1e-4
    print("epsilon = "+str(epsilon))
    
    # MLP parameters
    num_units = 50#4096
    print("num_units = "+str(num_units))
    n_hidden_layers = 0
    print("n_hidden_layers = "+str(n_hidden_layers))
    
    # Training parameters
    num_epochs = 500
    print("num_epochs = "+str(num_epochs))
    
    # BinaryOut
    activation = binary_net.binary_tanh_unit
    print("activation = binary_net.binary_tanh_unit")
    
    # BinaryConnect
    binary = True
    print("binary = "+str(binary))
    stochastic = False
    print("stochastic = "+str(stochastic))
    H = 1.
    print("H = "+str(H))
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    
    # Decaying LR 
    LR_start = .003
    print("LR_start = "+str(LR_start))
    LR_fin = 0.0000003
    print("LR_fin = "+str(LR_fin))
    LR_decay = (LR_fin/LR_start)**(1./num_epochs)
    print("LR_decay = "+str(LR_decay))
    
    save_path = "iris_shallow.npz"
    print("save_path = "+str(save_path))
    
    shuffle_parts = 1
    print("shuffle_parts = "+str(shuffle_parts))


    print('Building the MLP...') 
    # Prepare Theano variables for inputs and targets
    input = T.tensor4('inputs')
    target = T.matrix('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    mlp = lasagne.layers.InputLayer(
            shape=(None, 1, 1, 4),
            input_var=input)
    """
    for k in range(n_hidden_layers):

        mlp = binary_net.DenseLayer(
                mlp, 
                b=None,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=num_units)                  
        
        mlp = lasagne.layers.BatchNormLayer(
                mlp,
                epsilon=epsilon, 
                alpha=alpha)
        
        mlp = lasagne.layers.NonlinearityLayer(
                mlp,
                nonlinearity=activation)
    """
    mlp = binary_net.DenseLayer(
                mlp, 
                b=None,
                binary=binary,
                stochastic=stochastic,
                H=H,
                W_LR_scale=W_LR_scale,
                nonlinearity=lasagne.nonlinearities.identity,
                num_units=1)    
    
    mlp = lasagne.layers.BatchNormLayer(
            mlp,
            epsilon=epsilon, 
            alpha=alpha)
    
    #all_params =  lasagne.layers.get_all_params(mlp)
    #print all_params
    #assert False
    train_output = lasagne.layers.get_output(mlp, deterministic=False)
    
    # squared hinge loss
    loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))
    
    if binary:
        
        # W updates
        W = lasagne.layers.get_all_params(mlp, binary=True)
        W_grads = binary_net.compute_grads(loss,mlp)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        updates = binary_net.clipping_scaling(updates,mlp)
        
        # other parameters updates
        params = lasagne.layers.get_all_params(mlp, trainable=True, binary=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())
        
    else:
        params = lasagne.layers.get_all_params(mlp, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)

    test_output = lasagne.layers.get_output(mlp, deterministic=True)
    test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
    #test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX) #multi-class
    test_err = T.mean(T.neq(T.gt(test_output, theano.shared(0.0)), target),dtype=theano.config.floatX)
    
    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary) 
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], [test_loss, test_err])

    print('Loading Iris dataset...')
    idx = range(150)
    random.shuffle(idx)

    #idx_tr = idx[:110]
    #idx_va = idx[110:130] # split 20 samples for validation
    idx_tr = idx[:120]
    idx_va = idx[120:] # validation set is the same with the test set
    idx_te = idx[120:]

    train_set = Iris()
    valid_set = Iris()
    test_set = Iris()
    data = Iris()

    print "data.X.shape: ", data.X.shape # (150,4)
    print "data.y.shape: ", data.y.shape # (150,1)
    #print "data.X", data.X # the three classes are not shuffled
    #print "data.y", data.y # the three classes are not shuffled

    train_set.X, train_set.y = data.X[idx_tr, :], data.y[idx_tr, :]
    valid_set.X, valid_set.y = data.X[idx_va, :], data.y[idx_va, :]
    test_set.X, test_set.y = data.X[idx_te, :], data.y[idx_te, :]
    """
    train_set.X = train_set.X.reshape(-1, 1, 1, 4)
    valid_set.X = valid_set.X.reshape(-1, 1, 1, 4)
    test_set.X = test_set.X.reshape(-1, 1, 1, 4)
    """
    train_set.X =0.25* train_set.X.reshape(-1, 1, 1, 4) - 1.
    valid_set.X = 0.25* valid_set.X.reshape(-1, 1, 1, 4) - 1.
    test_set.X = 0.25* test_set.X.reshape(-1, 1, 1, 4) - 1.
    """
    train_set.X = bina(train_set.X)
    valid_set.X = bina(valid_set.X)
    test_set.X = bina(test_set.X)
    """
    train_set.X = np.float32(train_set.X)
    valid_set.X = np.float32(valid_set.X)
    test_set.X = np.float32(test_set.X)

    print(train_set.X.shape)
    #print train_set.X
    print "min: ",np.amin(train_set.X)
    print "max: ",np.amax(train_set.X)
    print(train_set.y.shape)
    #assert False

    # Now we recoginze only the first class
    first = train_set.y<1
    rest = train_set.y>0
    train_set.y[first] = 1.0
    train_set.y[rest] =  0.0
    first = valid_set.y<1
    rest = valid_set.y>0
    valid_set.y[first] = 1.0
    valid_set.y[rest] =  0.0
    test_set.y[first] = 1.0
    test_set.y[rest] =  0.0
    #print "single class train_set.y: ", train_set.y
    """
    # flatten targets
    train_set.y = np.hstack(train_set.y)
    valid_set.y = np.hstack(valid_set.y)
    test_set.y = np.hstack(test_set.y)
    """
    # Onehot the targets
    """
    train_set.y = np.float32(np.eye(3)[train_set.y])    
    valid_set.y = np.float32(np.eye(3)[valid_set.y])
    test_set.y = np.float32(np.eye(3)[test_set.y])
    """
    """
    train_set.y = np.float32(np.eye(1)[train_set.y])    
    valid_set.y = np.float32(np.eye(1)[valid_set.y])
    test_set.y = np.float32(np.eye(1)[test_set.y])
    """

    train_set.y = np.float32(train_set.y)    
    valid_set.y = np.float32(valid_set.y)
    test_set.y = np.float32(test_set.y)

    print(train_set.X.shape)
    print(train_set.y.shape)
    
    
    print('Training...')
    binary_net.train(
            train_fn,val_fn,
            mlp,
            batch_size,
            LR_start,LR_decay,
            num_epochs,
            train_set.X,train_set.y,
            valid_set.X,valid_set.y,
            test_set.X,test_set.y,
            save_path,
            shuffle_parts)
    
    """
    #==== JH: Test the trained parameters ====#
    print("Loading the trained parameters and binarizing the weights...")
    sys.stdout.flush()
    
    # Load parameters
    with np.load(save_path) as f:
    #with np.load('trained_iris_1n.npz') as f:
        param_values = [f['arr_%d' % i] for i in range(len(f.files))]
    lasagne.layers.set_all_param_values(mlp, param_values)
    
    #==== JH: Binarize the weights ====#
    params = lasagne.layers.get_all_params(mlp)
    
    # Go through our own binarization to verify
    for idx5, param in enumerate(params):
        if param.name == "W":
            param_4D = param.get_value()
            param_4D = bina(param_4D) #==== JH: Important! We perform SVD on binarized filters ====# 

            param.set_value(param_4D)
    
    print('Testing...')
    sys.stdout.flush()
    
    test(
            val_fn,
            batch_size,
            test_set.X,test_set.y)
    """
