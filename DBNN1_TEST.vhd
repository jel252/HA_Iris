-- Test-bench for a deep binarized neural network
-- A deep neural network for 3 classes 
-- By Jeng-Hau Lin
-- 2017-05-22

----------------------------------------
-- Include libraries
LIBRARY ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use STD.textio.all;

----------------------------------------
-- A test-bench has no port
ENTITY DBNN1_TEST IS
END DBNN1_TEST;
 
----------------------------------------
-- Use mixed modelings to implement the test-bench
ARCHITECTURE beha OF DBNN1_TEST IS 
  type W0_t is array (49 DOWNTO 0) of std_logic_vector(3 DOWNTO 0);
  signal TW0: W0_t := (others=>(others=>'0'));
  type W1_t is array (2 DOWNTO 0) of std_logic_vector(49 DOWNTO 0);
  signal TW1: W1_t := (others=>(others=>'0'));

  type kh0_t is array (49 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
  signal Tk0: kh0_t := (others=>(others=>'0'));
  signal Th0: kh0_t := (others=>(others=>'0'));
  type kh1_t is array (2 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
  signal Tk1: kh1_t := (others=>(others=>'0'));
  signal Th1: kh1_t := (others=>(others=>'0'));

  type X_t is array (0 TO 29, 3 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
  signal test_X: X_t := (others=>(others=>"00000000"));

  type y_t is array (0 TO 29) of integer;
  signal test_y: y_t := (others=>0);

  COMPONENT NPU1
  port(CLK: in std_logic;
         A: in std_logic_vector(31 DOWNTO 0);
         W: in std_logic_vector(3 DOWNTO 0);
         k: in std_logic_vector(7 DOWNTO 0);
         h: in std_logic_vector(7 DOWNTO 0);
         P: out std_logic);
  end COMPONENT;

  COMPONENT NPU2
  port(CLK: in std_logic;
         A: in  std_logic_vector(49 DOWNTO 0);
         W: in  std_logic_vector(49 DOWNTO 0);
         k: in  std_logic_vector(7 DOWNTO 0);
         h: in  std_logic_vector(7 DOWNTO 0);
         P: out std_logic_vector(7 DOWNTO 0));
  end COMPONENT;

  --signal hid_act: std_logic_vector(49 DOWNTO 0) := (others=>'0');
  signal hid_act: std_logic_vector(0 TO 49) := (others=>'0');

  constant Tpe: time := 2 ns;
  signal TCLK: std_logic:= '0';
  signal TA: std_logic_vector(31 DOWNTO 0);
    
  type TP_t is array (2 DOWNTO 0) of std_logic_vector(7 DOWNTO 0);
  signal TP: TP_t := (others=>(others=>'0'));

  signal pred: integer := 0;

  impure FUNCTION read_file(
  str: string;
  str_len: integer;
  nbit: integer;
  matrix_w: integer;
  matrix_h: integer)
  return std_logic_vector is
    variable  bin_value: std_logic_vector(nbit*(matrix_w)*matrix_h-1 DOWNTO 0);
    file file_pointer: text;
    variable line_content: string(1 to nbit*matrix_w);
    variable line_num: line;
    variable j: integer := 0; -- character counter in the file
    variable r: integer := 0; -- row counter in the file
    variable char: character; 
  BEGIN
    file_open(file_pointer,str(1 TO str_len),READ_MODE);	  
    while not endfile(file_pointer) loop 
      readline (file_pointer,line_num);
      read(line_num,line_content);  
      for j in 1 to (nbit*matrix_w) loop
        char := line_content(j);
        if(char = '0') then
          bin_value((nbit*matrix_w*r) + nbit*matrix_w-j) := '0';
        else
          bin_value((nbit*matrix_w*r) + nbit*matrix_w-j) := '1';
        end if;	
      end loop;
      r := r + 1;
    end loop;
    file_close(file_pointer);
    return bin_value;
  end FUNCTION read_file;

  impure FUNCTION read_file_int(
  str: string;
  str_len: integer;
  nbit: integer;
  matrix_w: integer;
  matrix_h: integer)
  return y_t is
    variable  bin_value: y_t:=(others=>0);
    file file_pointer: text;
    variable line_content: string(1 to nbit*matrix_w);
    variable line_num: line;
    variable j: integer := 0; -- character counter in the file
    variable r: integer := 0; -- row counter in the file
    variable char: character; 
  BEGIN
    file_open(file_pointer,str(1 TO str_len),READ_MODE);	  
    while not endfile(file_pointer) loop 
      readline (file_pointer,line_num);
      read(line_num,line_content);  
        char := line_content(1);
        if(char = '0') then
          bin_value(r) := 0;
        elsif(char = '1') then
          bin_value(r) := 1;
        else
          bin_value(r) := 2;
        end if;	
      r := r + 1;
    end loop;
    file_close(file_pointer);
    return bin_value;
  end FUNCTION read_file_int;
BEGIN
  -- Clock generator
  clk_gen : PROCESS
  BEGIN
    TCLK <= not TCLK;
    wait for Tpe/2;
  end PROCESS clk_gen;

  -- Read the trained model parameters and the dataset
  read_weights: PROCESS  
    -- Model parameter
    variable w0filename: string(1 to 28) := "data/model_deep/weights0.txt"; -- 28 is the lenght of the string
    variable w0_slv: std_logic_vector(1*4*50-1 DOWNTO 0);
    variable w1filename: string(1 to 28) := "data/model_deep/weights1.txt";
    variable w1_slv: std_logic_vector(1*50*3-1 DOWNTO 0);
    variable kh0filename: string(1 to 23) := "data/model_deep/kh0.txt";
    variable kh0_slv: std_logic_vector(8*2*50-1 DOWNTO 0) := (others=>'0');
    variable kh1filename: string(1 to 23) := "data/model_deep/kh1.txt";
    variable kh1_slv: std_logic_vector(8*2*3-1 DOWNTO 0) := (others=>'0');
    -- Dataset
    variable Xfilename: string(1 to 24) := "data/test_set/test_X.txt";
    variable test_X_slv: std_logic_vector(8*4*30-1 DOWNTO 0);
    variable yfilename: string(1 to 27) := "data/test_set/test_y_3c.txt";
    --variable test_y_slv: std_logic_vector(1*1*30-1 DOWNTO 0);
    variable r: integer := 0; -- row counter for matrix assignment
    variable c: integer := 0; -- column counter for matrix assignment
  BEGIN
    w0_slv := read_file(w0filename, 28, 1,4,50);
    for r in 0 TO 49 loop
      TW0(r) <= w0_slv(r*1*4+3 DOWNTO r*1*4+0);
    end loop;

    w1_slv := read_file(w1filename, 28, 1,50,3);
    for r in 0 TO 2 loop
      TW1(r) <= w1_slv(r*1*50+49 DOWNTO r*1*50+0);
    end loop;

    kh0_slv := read_file(kh0filename, 23, 8,2,50);
    for r in 0 TO 49 loop
      Tk0(r) <= kh0_slv(r*8*2+15 DOWNTO r*8*2+8); 
      Th0(r) <= kh0_slv(r*8*2+7 DOWNTO r*8*2+0);
    end loop;

    kh1_slv := read_file(kh1filename, 23, 8,2,3);
    for r in 0 TO 2 loop
      Tk1(r) <= kh1_slv(r*8*2+15 DOWNTO r*8*2+8); 
      Th1(r) <= kh1_slv(r*8*2+7 DOWNTO r*8*2+0);
    end loop;

    test_X_slv := read_file(Xfilename, 24, 8,4,30);
    test_y <= read_file_int(yfilename, 27, 1,1,30);
    for r in 0 TO 29 loop
      test_X(r,3) <= test_X_slv(r*8*4+31 DOWNTO r*8*4+24); -- the 4 features in 8-bit signed format
      test_X(r,2) <= test_X_slv(r*8*4+23 DOWNTO r*8*4+16);
      test_X(r,1) <= test_X_slv(r*8*4+15 DOWNTO r*8*4+8);
      test_X(r,0) <= test_X_slv(r*8*4+7  DOWNTO r*8*4+0);
    end loop;
    wait;
  end PROCESS read_weights;

  HIDDEN01: NPU1 port map (TCLK, TA, TW0( 0), Tk0( 0), Th0( 0), hid_act( 0));
  HIDDEN02: NPU1 port map (TCLK, TA, TW0( 1), Tk0( 1), Th0( 1), hid_act( 1));
  HIDDEN03: NPU1 port map (TCLK, TA, TW0( 2), Tk0( 2), Th0( 2), hid_act( 2));
  HIDDEN04: NPU1 port map (TCLK, TA, TW0( 3), Tk0( 3), Th0( 3), hid_act( 3));
  HIDDEN05: NPU1 port map (TCLK, TA, TW0( 4), Tk0( 4), Th0( 4), hid_act( 4));
  HIDDEN06: NPU1 port map (TCLK, TA, TW0( 5), Tk0( 5), Th0( 5), hid_act( 5));
  HIDDEN07: NPU1 port map (TCLK, TA, TW0( 6), Tk0( 6), Th0( 6), hid_act( 6));
  HIDDEN08: NPU1 port map (TCLK, TA, TW0( 7), Tk0( 7), Th0( 7), hid_act( 7));
  HIDDEN09: NPU1 port map (TCLK, TA, TW0( 8), Tk0( 8), Th0( 8), hid_act( 8));
  HIDDEN10: NPU1 port map (TCLK, TA, TW0( 9), Tk0( 9), Th0( 9), hid_act( 9));
  HIDDEN11: NPU1 port map (TCLK, TA, TW0(10), Tk0(10), Th0(10), hid_act(10));
  HIDDEN12: NPU1 port map (TCLK, TA, TW0(11), Tk0(11), Th0(11), hid_act(11));
  HIDDEN13: NPU1 port map (TCLK, TA, TW0(12), Tk0(12), Th0(12), hid_act(12));
  HIDDEN14: NPU1 port map (TCLK, TA, TW0(13), Tk0(13), Th0(13), hid_act(13));
  HIDDEN15: NPU1 port map (TCLK, TA, TW0(14), Tk0(14), Th0(14), hid_act(14));
  HIDDEN16: NPU1 port map (TCLK, TA, TW0(15), Tk0(15), Th0(15), hid_act(15));
  HIDDEN17: NPU1 port map (TCLK, TA, TW0(16), Tk0(16), Th0(16), hid_act(16));
  HIDDEN18: NPU1 port map (TCLK, TA, TW0(17), Tk0(17), Th0(17), hid_act(17));
  HIDDEN19: NPU1 port map (TCLK, TA, TW0(18), Tk0(18), Th0(18), hid_act(18));
  HIDDEN20: NPU1 port map (TCLK, TA, TW0(19), Tk0(19), Th0(19), hid_act(19));
  HIDDEN21: NPU1 port map (TCLK, TA, TW0(20), Tk0(20), Th0(20), hid_act(20));
  HIDDEN22: NPU1 port map (TCLK, TA, TW0(21), Tk0(21), Th0(21), hid_act(21));
  HIDDEN23: NPU1 port map (TCLK, TA, TW0(22), Tk0(22), Th0(22), hid_act(22));
  HIDDEN24: NPU1 port map (TCLK, TA, TW0(23), Tk0(23), Th0(23), hid_act(23));
  HIDDEN25: NPU1 port map (TCLK, TA, TW0(24), Tk0(24), Th0(24), hid_act(24));
  HIDDEN26: NPU1 port map (TCLK, TA, TW0(25), Tk0(25), Th0(25), hid_act(25));
  HIDDEN27: NPU1 port map (TCLK, TA, TW0(26), Tk0(26), Th0(26), hid_act(26));
  HIDDEN28: NPU1 port map (TCLK, TA, TW0(27), Tk0(27), Th0(27), hid_act(27));
  HIDDEN29: NPU1 port map (TCLK, TA, TW0(28), Tk0(28), Th0(28), hid_act(28));
  HIDDEN30: NPU1 port map (TCLK, TA, TW0(29), Tk0(29), Th0(29), hid_act(29));
  HIDDEN31: NPU1 port map (TCLK, TA, TW0(30), Tk0(30), Th0(30), hid_act(30));
  HIDDEN32: NPU1 port map (TCLK, TA, TW0(31), Tk0(31), Th0(31), hid_act(31));
  HIDDEN33: NPU1 port map (TCLK, TA, TW0(32), Tk0(32), Th0(32), hid_act(32));
  HIDDEN34: NPU1 port map (TCLK, TA, TW0(33), Tk0(33), Th0(33), hid_act(33));
  HIDDEN35: NPU1 port map (TCLK, TA, TW0(34), Tk0(34), Th0(34), hid_act(34));
  HIDDEN36: NPU1 port map (TCLK, TA, TW0(35), Tk0(35), Th0(35), hid_act(35));
  HIDDEN37: NPU1 port map (TCLK, TA, TW0(36), Tk0(36), Th0(36), hid_act(36));
  HIDDEN38: NPU1 port map (TCLK, TA, TW0(37), Tk0(37), Th0(37), hid_act(37));
  HIDDEN39: NPU1 port map (TCLK, TA, TW0(38), Tk0(38), Th0(38), hid_act(38));
  HIDDEN40: NPU1 port map (TCLK, TA, TW0(39), Tk0(39), Th0(39), hid_act(39));
  HIDDEN41: NPU1 port map (TCLK, TA, TW0(40), Tk0(40), Th0(40), hid_act(40));
  HIDDEN42: NPU1 port map (TCLK, TA, TW0(41), Tk0(41), Th0(41), hid_act(41));
  HIDDEN43: NPU1 port map (TCLK, TA, TW0(42), Tk0(42), Th0(42), hid_act(42));
  HIDDEN44: NPU1 port map (TCLK, TA, TW0(43), Tk0(43), Th0(43), hid_act(43));
  HIDDEN45: NPU1 port map (TCLK, TA, TW0(44), Tk0(44), Th0(44), hid_act(44));
  HIDDEN46: NPU1 port map (TCLK, TA, TW0(45), Tk0(45), Th0(45), hid_act(45));
  HIDDEN47: NPU1 port map (TCLK, TA, TW0(46), Tk0(46), Th0(46), hid_act(46));
  HIDDEN48: NPU1 port map (TCLK, TA, TW0(47), Tk0(47), Th0(47), hid_act(47));
  HIDDEN49: NPU1 port map (TCLK, TA, TW0(48), Tk0(48), Th0(48), hid_act(48));
  HIDDEN50: NPU1 port map (TCLK, TA, TW0(49), Tk0(49), Th0(49), hid_act(49));
  OUTPUT1: NPU2 port map (TCLK, hid_act, TW1(0), Tk1(0), Th1(0), TP(0));
  OUTPUT2: NPU2 port map (TCLK, hid_act, TW1(1), Tk1(1), Th1(1), TP(1));
  OUTPUT3: NPU2 port map (TCLK, hid_act, TW1(2), Tk1(2), Th1(2), TP(2));

  deep: PROCESS
    variable error_count: integer := 0;
    variable tmp: std_logic_vector(7 DOWNTO 0):=(others=>'0');
  BEGIN

    wait for 10 ns; -- Wait for all file access to finish

    for i in 0 to 29 loop -- iterate over all 30 samples in the test set
      TA(31 DOWNTO 24) <= Test_X(i,3);
      TA(23 DOWNTO 16) <= Test_X(i,2);
      TA(15 DOWNTO 8) <= Test_X(i,1);
      TA(7 DOWNTO 0) <= Test_X(i,0);
      
      wait for 2 ns;

      wait for 1 ns;
      pred <= 0;
      tmp := TP(0);
      for i in 1 to 2 loop
        if to_integer(signed(tmp)) < to_integer(signed(TP(i))) then
          tmp := TP(i);
          pred <= i;
        end if;
      end loop;

      wait for 1 ns;

      if (pred /= test_y(i)) then -- Check the classification correctness
        error_count := error_count +1;
      end if;
      report "Target: " & integer'image(test_y(i)) & ", Predicted: " & integer'image(pred);
    end loop;
    -- 6.67% error rate
    report "Total error count: " & integer'image(error_count) & "/30"; -- the error rate should be 5/30

    wait;
  end PROCESS deep;

end beha;
